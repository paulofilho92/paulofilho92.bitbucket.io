# Unigran Explorer
Simple VR explorer for Unigran.

### Running

Checkout this repo, install dependencies, then start livereload server.

### How-to

Click/gaze into panels/arrows in the scene to move to another location, or click/gaze into question marks to view some information about a certain location.